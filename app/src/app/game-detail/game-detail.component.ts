import { Component, OnInit } from '@angular/core';

enum MaterialTypes {
  Organic,
  Paper,
  Metal,
  Plastic,
  Glass,
}

interface Item {
  id: number;
  name: string;
  points: number;
  checked: boolean;
}
@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss']
})
export class GameDetailComponent implements OnInit {

  items: Item[] = [
    {
      id: MaterialTypes.Organic,
      name: 'Lixo orgânico',
      points: 20,
      checked: false,
    },
    {
      id: MaterialTypes.Paper,
      name: 'Papel',
      points: 10,
      checked: false,
    },
    {
      id: MaterialTypes.Metal,
      name: 'Metal',
      points: 40,
      checked: false,
    },
    {
      id: MaterialTypes.Plastic,
      name: 'Plástico',
      points: 100,
      checked: false,
    },
    {
      id: MaterialTypes.Glass,
      name: 'Vidro',
      points: 200,
      checked: false,
    }
  ];

  constructor() {}

  ngOnInit(): void {}
}
