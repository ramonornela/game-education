import { Component, OnInit } from '@angular/core';

interface User {
  name: string;
  points: number;
  avatar: string;
}
@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {

  users: User[] = [];

  constructor() { }

  ngOnInit(): void {
    this.users = [
      {
        name: 'player2',
        points: 1000,
        avatar: 'https://material.angular.io/assets/img/examples/shiba1.jpg',
      },
      {
        name: 'player1',
        points: 30,
        avatar: 'https://material.angular.io/assets/img/examples/shiba1.jpg',
      },
    ];
  }

}
