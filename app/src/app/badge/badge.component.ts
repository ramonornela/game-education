import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss']
})
export class BadgeComponent implements OnInit {

  badges: string[] = [];

  constructor() { }

  ngOnInit(): void {
    this.badges = [
      'Recycler Start',
      'Recycler',
      'Early adopter',
      'Recycler Advanced',
      'Recycler Master',
    ];
  }

}
