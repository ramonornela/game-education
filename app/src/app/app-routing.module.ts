import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { GameComponent } from './game/game.component';
import { GameDetailComponent } from './game-detail/game-detail.component';
import { BadgeComponent } from './badge/badge.component';
import { RankingComponent } from './ranking/ranking.component';
import { UserDetailComponent } from './user-detail/user-detail.component';

const routes: Routes = [
  {
    path: 'game',
    component: GameComponent,
  },
  {
    path: 'game-detail',
    component: GameDetailComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'ranking',
    component: RankingComponent,
  },
  {
    path: 'user-detail',
    component: UserDetailComponent,
  },
  {
    path: 'badge',
    component: BadgeComponent,
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'game'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
